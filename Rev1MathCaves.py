



#Math Caves Verson 1.0.0
#Programmed by Rowan Cutler for Hobson Lane's CISC 179


''' A fun math game that teaches you.. well, math! 
In the vault I heard theres a map that has the power to find
 the Ark of the Covenant, Good Luck my friend! I hope you payed enough 
 attention in math class, cause I did'nt even though im a 
 purty good coder ;) --Rowan C.  '''

import time
import random

time.sleep(1)
RightAnswer = 0
Problem_Saves = {"Add" : [], "Sub" : [], "Mult" : [], "Div" : []}
      
print("Hello, welcome to Math Caves!")
time.sleep(1)
Start_Countdown = 5

for Number in range(Start_Countdown):
    print("Game starting in " + str(Start_Countdown))
    Start_Countdown = Start_Countdown - 1
    time.sleep(1)

print("Let the games begin!")    
time.sleep(0.1)
cave1done = False
cave2done = False
cave3done = False
cave4done = False



problemCorrect = 0
def addition():  
 beginningTime = int(time.time())
 NumberOne = random.randint(1,10)
 NumberTwo = random.randint(1,10)
 Trial = input("What is " + str(NumberOne) + " + " + str(NumberTwo) + "?")
 endingTime = int(time.time())

 if int(endingTime - beginningTime ) > 5: 
     print("Sorry it took you longer then five seconds to complete this problem.On to the next!")
     
 else:
      if int(NumberOne+NumberTwo) == int(Trial):
          print("Thats correct!")
          global problemCorrect 
          
          problemCorrect = problemCorrect + 1
      else:
          print("Thats incorrect, It was added to your incorrect list")
          Problem_Saves["Add"].append(str(NumberOne) + " " +str(NumberTwo))
          
 
def subtraction():  
 beginningTime = int(time.time())
 NumberOne = random.randint(1,10)
 NumberTwo = random.randint(1,10)
 NumberThree = NumberOne + NumberTwo
 Trial = input("What is " + str(NumberThree) + " - " + str(NumberTwo) + "?")
 endingTime = int(time.time())

 if int(endingTime - beginningTime ) > 5: 
     print("Sorry it took you longer then five seconds to complete this problem.On to the next!")
     
 else:
      if int(NumberOne) == int(Trial):
          print("Thats correct!")
          global problemCorrect 
          
          problemCorrect = problemCorrect + 1
      else:
          print("Thats incorrect, It was added to your incorrect list")
          Problem_Saves["Sub"].append(str(NumberThree) + " " +str(NumberTwo))
          
def multiplication():  
 beginningTime = int(time.time())
 NumberOne = random.randint(1,10)
 NumberTwo = random.randint(1,10)
 NumberThree = NumberOne * NumberTwo
 Trial = input("What is " + str(NumberOne) + " x " + str(NumberTwo) + "?")
 endingTime = int(time.time())
 
 if int(endingTime - beginningTime ) > 5: 
     print("Sorry it took you longer then five seconds to complete this problem.On to the next!")
     
 else:
      if int(NumberThree) == int(Trial):
          print("Thats correct!")
          global problemCorrect 
          
          problemCorrect = problemCorrect + 1
      else:
          print("Thats incorrect, It was added to your incorrect list")
          Problem_Saves["Mult"].append(str(NumberOne) + " " +str(NumberTwo))
def division():  
 beginningTime = int(time.time())
 NumberOne = random.randint(1,10)
 NumberTwo = random.randint(1,10)
 NumberThree = NumberOne * NumberTwo
 Trial = input("What is " + str(NumberThree) + " Divided by " + str(NumberTwo) + "?")
 endingTime = int(time.time())

 if int(endingTime - beginningTime ) > 5: 
     print("Sorry it took you longer then five seconds to complete this problem.On to the next!")
     
 else:
      if int(NumberOne) == int(Trial):
          print("Thats correct!")
          global problemCorrect 
          
          problemCorrect = problemCorrect + 1
      else:
          print("Thats incorrect, It was added to your incorrect list")
          Problem_Saves["Div"].append(str(NumberThree) + " " +str(NumberTwo))
          
while cave1done == False:    
 if problemCorrect > 9:
  cave1done = True


 if cave1done == False:    
    randomnumber = random.randint(1,4)
    if randomnumber == 1:
        addition()
    elif randomnumber  == 2:
        subtraction()
    elif randomnumber  == 3:
        division()
    elif randomnumber  == 4:
        multiplication()
          
 elif cave1done == True:
   if  str(Problem_Saves.values()) == "dict_values([[], [], [], []])":
     print("HOLY COW! YOU GOT EVERYTHING RIGHT! CONGRADULATIONS! YOU GOT THE TREASURE VAULT KEY!")
     print("You are now in the treasure vault, in the vault there is a map that contains real world courdnates leading to the Ark of the Covenant: 44.51462236561808, -64.29204752395907")  
  
   else:
    print("You have obtained a key to the master's chamber since you got some wrong, you will need to take the ultimate test")      
    Start_Countdown = 10
    time.sleep(5)
    print(" ")
    print(" ")
    print(" ")

 for Number in range(Start_Countdown):
  print("The masters chamber ultimate trial testing begins in " + str(Start_Countdown))
  Start_Countdown = Start_Countdown - 1
  time.sleep(1)
  print(" ")
  print(" ")
def AddTrial():
  for I in Problem_Saves["Add"]:
      print("Lets start with addition problems you missed")
      Numbers = I.split()  
      
      Trial = input("What is " + str(Numbers[0]) + " + " + str(Numbers[1]) + "?")
      global RightAnswer
      RightAnswer = int(Numbers[0]) + int(Numbers[1])
      print(RightAnswer)
  if RightAnswer == int(Trial):
             
        print("Thats correct! Your learning!")
               
  else: 
        print("Sorry, thats not correct! Start over.")
        AddTrial()

def LessTrial():
  for I in Problem_Saves["Sub"]:
      print("Lets do the Subtraction problems you missed")
      Numbers = I.split()  
      
      Trial = input("What is " + str(Numbers[0]) + " - " + str(Numbers[1]) + "?")
      global RightAnswer
      RightAnswer = int(Numbers[0]) - int(Numbers[1])
      
  if RightAnswer == int(Trial):
             
        print("Thats correct! Your learning!")
               
  else: 
        print("Sorry, thats not correct! Start over.")
        LessTrial()       

def MultTrial():
  for I in Problem_Saves["Mult"]:
      print("Lets do the Multiplication problems you missed")
      Numbers = I.split()  
      
      Trial = input("What is " + str(Numbers[0]) + " x " + str(Numbers[1]) + "?")
      global RightAnswer
      RightAnswer = int(Numbers[0]) * int(Numbers[1])

  if RightAnswer == int(Trial):
             
        print("Thats correct! Your learning!")
               
  else: 
        print("Sorry, thats not correct! Start over.")
        MultTrial()    
        
               
def DivTrial():
  for I in Problem_Saves["Div"]:
      print("Lets do the Division problems you missed")
      Numbers = I.split()  
      
      Trial = input("What is " + str(Numbers[0]) + " Divided by " + str(Numbers[1]) + "?")
      global RightAnswer
      RightAnswer = int(Numbers[0]) / int(Numbers[1])

  if RightAnswer == int(Trial):
             
        print("Thats correct! Your learning!")
               
  else: 
        print("Sorry, thats not correct! Start over.")
        DivTrial()    
AddTrial()
LessTrial() 
MultTrial()
DivTrial()
print("You are now in the treasure vault, in the vault there is a map that contains real world courdnates leading to the Ark of the Covenant: 44.51462236561808, -64.29204752395907") 

               
        
     


MultNumberOne = random.randint(1,12)
MultNumberTwo = random.randint(1,12)     